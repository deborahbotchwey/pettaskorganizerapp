package com.rgajardo.pettaskorganizer;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Menu;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.util.ArrayList;

public class drawer extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;
    boolean isFABOpen;
    FloatingActionButton fabAdd;
    FloatingActionButton fabAddEmployee;
    FloatingActionButton fabAddPet;
    FloatingActionButton fabAddTask;
    static ArrayList<String> pets = new ArrayList<>();
    static ArrayList<String> employees = new ArrayList<>();
    public static Context mContext;
    SQLiteHelper db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mContext = getApplicationContext();
        db = new SQLiteHelper(this);

        fabAdd = findViewById(R.id.fabAdd);
        fabAddEmployee = findViewById(R.id.fabAddEmployee);
        fabAddPet = findViewById(R.id.fabAddPet);
        fabAddTask = findViewById(R.id.fabAddTask);



        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isFABOpen){
                    showFABMenu();
                }else{
                    closeFABMenu();
                }
            }

        });

        fabAddPet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(drawer.this,
                        addPet.class);
                closeFABMenu();
                startActivity(intent);
            }
        });

        fabAddEmployee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(drawer.this,
                        AddEmployee.class);
                closeFABMenu();
                startActivity(intent);
            }
        });

        fabAddTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(drawer.this,
                        AddTask.class);
                closeFABMenu();
                startActivity(intent);
            }
        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);



    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.drawer, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    private void showFABMenu(){
        isFABOpen=true;
        fabAddPet.animate().translationY(-getResources().getDimension(R.dimen.standard_55));
        fabAddEmployee.animate().translationY(-getResources().getDimension(R.dimen.standard_105));
        fabAddTask.animate().translationY(-getResources().getDimension(R.dimen.standard_155));
    }

    private void closeFABMenu(){
        isFABOpen=false;
        fabAddEmployee.animate().translationY(0);
        fabAddPet.animate().translationY(0);
        fabAddTask.animate().translationY(0);
    }
}

