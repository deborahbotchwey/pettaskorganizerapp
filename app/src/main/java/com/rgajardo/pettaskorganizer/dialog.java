package com.rgajardo.pettaskorganizer;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentTransaction;

import com.rgajardo.pettaskorganizer.R;
import com.rgajardo.pettaskorganizer.ui.home.HomeFragment;

import java.util.ArrayList;
import java.util.EventListener;

public class dialog extends DialogFragment {

    SQLiteHelper db ;
    Spinner spinnerNames;
    boolean bool = false;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        db = new SQLiteHelper(getActivity());

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        String StringserValue = getArguments().getString("data");
        final String[] arr = StringserValue.split(" ");
        final String type = arr[0];
        final String pet = arr[1];
        final String time = arr[3];

        final LayoutInflater inflater = getActivity().getLayoutInflater();
        final View view = inflater.inflate(R.layout.layout_dialog,null);
        builder.setView(view)
                .setMessage(StringserValue)
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setPositiveButton("Done", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String name = spinnerNames.getSelectedItem().toString();
                       db.addDataTasksDone(pet,type,name,time);
                       db.deleteTask(pet,type,time);
                    }
                });
        spinnerNames = view.findViewById(R.id.employeeTaskDone);
        Cursor employees = db.getDataEmployee();
        ArrayList<String> data = new ArrayList<>();
        while(employees.moveToNext()){
            data.add(employees.getString(1)+" "+employees.getString(2));
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),android.R.layout.simple_spinner_item,data);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerNames.setAdapter(adapter);



        return  builder.create();
    }

    static public dialog newInstance (String data){
        dialog d = new dialog();
        Bundle args = new Bundle();
        args.putString("data",data);
        d.setArguments(args);
        return d;
    }
    public boolean doneClick (){
        return bool;
    }
}
