package com.rgajardo.pettaskorganizer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class addPet extends AppCompatActivity {

    SQLiteHelper db;
    EditText name;
    RadioGroup types;
    RadioButton radioButton;
    Button btn;
    String type = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_pet);

        db = new SQLiteHelper(this);
        name = findViewById(R.id.txtInputPetName);
        types = findViewById(R.id.radioGroupPEtTypes);
        btn = findViewById(R.id.btnAddPetReady);

        types.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                radioButton = findViewById(checkedId);
                type = radioButton.getText().toString();
               // Toast.makeText(addPet.this,type,Toast.LENGTH_LONG).show();
            }
        });

    }

    public void addpet(View view) {

        if (name.getText().toString().matches("") || type == "" ){
            Toast.makeText(this,"Complete all fields",Toast.LENGTH_LONG).show();
        }
        else{
            String petname = name.getText().toString();
            db.addDataPet(petname, type);
            Toast.makeText(this,"new pet added successfully",Toast.LENGTH_LONG).show();
            Intent intent = new Intent(addPet.this, drawer.class);
            startActivity(intent);
        }
    }
}
