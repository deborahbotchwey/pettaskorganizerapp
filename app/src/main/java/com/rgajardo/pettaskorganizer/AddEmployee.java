package com.rgajardo.pettaskorganizer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddEmployee extends AppCompatActivity {

    SQLiteHelper db;
    EditText lastName;
    EditText Name;
    Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_employee);

        lastName = findViewById(R.id.editTextEmployeeLastName);
        button = findViewById(R.id.buttonAddEmployee);
        Name = findViewById(R.id.editTextEmployeeName);
        db = new SQLiteHelper(this);

    }

    public void addEmployee (View view){
        if (Name.getText().toString().matches("") || lastName.getText().toString().matches("")){
            Toast.makeText(this,"Complete all fields",Toast.LENGTH_LONG).show();
        }
        else{
            db.addDataEmployee(Name.getText().toString(), lastName.getText().toString());
            Toast.makeText(this,"new employee added successfully",Toast.LENGTH_LONG).show();
            Intent intent = new Intent(AddEmployee.this, drawer.class);
            startActivity(intent);
        }
    }
}


