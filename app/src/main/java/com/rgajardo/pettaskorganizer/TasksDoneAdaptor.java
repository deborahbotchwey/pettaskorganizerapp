package com.rgajardo.pettaskorganizer;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.LinkedList;

public class TasksDoneAdaptor extends
        RecyclerView.Adapter<TasksDoneAdaptor.ViewHolder> {

    LinkedList<String> mData;
    FragmentManager mFm;

    public TasksDoneAdaptor(LinkedList<String> wordList, FragmentManager fm) {
        mData = wordList;
        mFm = fm;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final CheckBox cbox;

        final TasksDoneAdaptor mAdapter;

        public ViewHolder(View itemView, TasksDoneAdaptor adapter) {
            super(itemView);
            cbox = itemView.findViewById(R.id.chechBoxtaskdone);
            this.mAdapter = adapter;
        }
    }

    @NonNull
    @Override
    public TasksDoneAdaptor.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mItemView = (CheckBox) LayoutInflater.from(parent.getContext()).inflate(R.layout.taskitemdone, parent, false);
        TasksDoneAdaptor.ViewHolder vh = new TasksDoneAdaptor.ViewHolder(mItemView, this);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull TasksDoneAdaptor.ViewHolder holder, int position) {
        final String mCurrent = mData.get(position);
        holder.cbox.setText(mCurrent);
        holder.cbox.setChecked(true);
    }

    public void openDialog(String data) {
        DialogFragment dia = dialog.newInstance(data);
        dia.show(mFm, "done");
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}
