package com.rgajardo.pettaskorganizer;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

import static java.lang.Integer.parseInt;

public class SQLiteHelper extends SQLiteOpenHelper {

    private static final String DB = "db";

    public SQLiteHelper(Context context) {
        super(context,DB, null,1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String emp = "CREATE TABLE IF NOT EXISTS employee_table (id INTEGER PRIMARY KEY, name VARCHAR, lastName VARCHAR)";
        String pet = "CREATE TABLE IF NOT EXISTS pets_table (id INTEGER PRIMARY KEY, name VARCHAR, type VARCHAR)";
        String tasks = "CREATE TABLE IF NOT EXISTS tasks_table (id INTEGER PRIMARY KEY, pet VARCHAR, tasktype VARCHAR, time VARCHAR, finished boolean)";
        String tasksDone = "CREATE TABLE IF NOT EXISTS tasksDone_table (id INTEGER PRIMARY KEY, pet VARCHAR, tasktype VARCHAR, time VARCHAR,  employee VARCHAR)";

        db.execSQL(emp);
        db.execSQL(pet);
        db.execSQL(tasks);
        db.execSQL(tasksDone);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
       db.execSQL("DROP TABLE IF EXISTS employee_table");
        db.execSQL("DROP TABLE IF EXISTS pets_table");
        db.execSQL("DROP TABLE IF EXISTS tasks_table");
        db.execSQL("DROP TABLE IF EXISTS tasksDone_table");
    }

    public boolean addDataEmployee (String name, String lastName ){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("name",name);
        cv.put("lastName", lastName);
        Log.i("add data", "Employee name: "+ name +" to employee_table");

        long result = db.insert("employee_table",null,cv);

        if (result ==-1){
            return false;
        }else {
            return true;
        }
    }

    public boolean addDataPet (String name, String type ){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("name",name);
        cv.put("type", type);

        long result = db.insert("pets_table",null,cv);

        if (result ==-1){
            return false;
        }else {
            return true;
        }
    }

    public boolean addDataTask (String pet, String taskstype , String time){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("pet",pet);
        cv.put("tasktype", taskstype);
        cv.put("time",time);
        cv.put("finished",false);


        long result = db.insert("tasks_table",null,cv);
//bool finished
        if (result ==-1){
            return false;
        }else {
            return true;
        }
    }

    public boolean addDataTasksDone (String pet, String taskstype , String employee, String time){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("pet",pet);
        cv.put("tasktype", taskstype);
        cv.put("time",time);
        cv.put("employee",employee);

        long result = db.insert("tasksDone_table",null,cv);

        if (result ==-1){
            return false;
        }else {
            return true;
        }
    }

    public Cursor getDataEmployee(){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM employee_table";
        Cursor data = db.rawQuery(query,null);
        return data;
    }

    public Cursor getDataPets(){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM pets_table";
        Cursor data = db.rawQuery(query,null);
        return data;
    }

    public Cursor getDataTasks(){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM tasks_table";
        Cursor data = db.rawQuery(query,null);
        return data;
    }

    public Cursor getDataTasksDone(){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM tasksDone_table";
        Cursor data = db.rawQuery(query,null);
        return data;
    }

    public boolean deleteTask(String pet,String type,String time){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DELETE FROM tasks_table WHERE pet='"+pet+"' AND tasktype='"+type+"' AND time='"+time+"'";
        db.execSQL(query);
        return true;
    }

    public boolean update (String pet, String tasktype, String time){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        String query = "SELECT id FROM tasks_table WHERE pet='"+pet+"' AND tasktype='"+tasktype+"' AND time='"+time+"'";
        Cursor id = db.rawQuery(query,null);
        Integer ID = 0;
        while(id.moveToNext()){
            ID = Integer.parseInt(id.getString(0));
        }
        cv.put("pet",pet);
        cv.put("tasktype", tasktype);
        cv.put("time",time);
        cv.put("finished",true);

        long result = db.update("tasks_table",cv,"id="+ID,null);

        if (result ==-1){
            return false;
        }else {
            return true;
        }

    }



}
