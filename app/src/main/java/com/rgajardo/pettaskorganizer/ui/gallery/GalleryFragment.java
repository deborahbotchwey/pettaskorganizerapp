package com.rgajardo.pettaskorganizer.ui.gallery;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rgajardo.pettaskorganizer.R;
import com.rgajardo.pettaskorganizer.SQLiteHelper;
import com.rgajardo.pettaskorganizer.TasksDoneAdaptor;
import com.rgajardo.pettaskorganizer.WordListAdapter;
import com.rgajardo.pettaskorganizer.ui.home.HomeViewModel;

import java.util.LinkedList;

public class GalleryFragment extends Fragment {

    private GalleryViewModel galleryViewModel;
    private RecyclerView mmRecyclerView;

    SQLiteHelper db;
    Context context;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        galleryViewModel =
                ViewModelProviders.of(this).get(GalleryViewModel.class);
        View root = inflater.inflate(R.layout.tasksdone_layout, container, false);

        context = container.getContext();
        db = new SQLiteHelper(getActivity());

        // Get a handle to the RecyclerView.
        mmRecyclerView = root.findViewById(R.id.LVDoneTasks);
        // Give the RecyclerView a default layout manager.
        mmRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        Cursor task = db.getDataTasksDone();
        LinkedList<String> tasks = new LinkedList<>();

        while(task.moveToNext()){
            String taskType;
            if (task.getString(2).matches( "Feed"))
                taskType = "Fed";
            else if (task.getString(2).matches("Clean"))
                taskType = "Cleaned";
            else if (task.getString(2).matches("Walk/Entertain"))
                taskType = "Walked/Entertained";

            tasks.add(task.getString(1) +" "+ task.getString(2)+" by "+task.getString(4)+" at "+task.getString(3));
        }

        final FragmentManager fm = getFragmentManager();
        // Create an adapter and supply the data to be displayed.
        TasksDoneAdaptor mAdapter = new TasksDoneAdaptor(tasks, fm);
        // Connect the adapter with the RecyclerView.
        mmRecyclerView.setAdapter(mAdapter);

        return root;
    }
}
