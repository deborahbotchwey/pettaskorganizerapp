package com.rgajardo.pettaskorganizer.ui.home;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rgajardo.pettaskorganizer.R;
import com.rgajardo.pettaskorganizer.SQLiteHelper;
import com.rgajardo.pettaskorganizer.WordListAdapter;

import java.util.LinkedList;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private RecyclerView mRecyclerView;
    SQLiteHelper db;
    Context context;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {


        context = container.getContext();
        db = new SQLiteHelper(getActivity());
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.activity_main, container, false);

        // Get a handle to the RecyclerView.
        mRecyclerView = root.findViewById(R.id.LVTodoTasks);
        // Give the RecyclerView a default layout manager.
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        //Get data about employee
        Cursor data = db.getDataEmployee();
        data.moveToFirst();
        while(data.moveToNext()){
            Log.i("get employee names: ", data.getString(0) +" "+data.getString(1) +" "+data.getString(2));
        }

        Cursor task = db.getDataTasks();
        LinkedList<String> tasks = new LinkedList<>();

        while(task.moveToNext()){
                tasks.add(task.getString(2) +" "+ task.getString(1)+" at "+task.getString(3));
                Log.i("data",this.toString());
        }


        final FragmentManager fm = getFragmentManager();
       // Create an adapter and supply the data to be displayed.
        WordListAdapter mAdapter = new WordListAdapter(tasks, fm);

       // Connect the adapter with the RecyclerView.
        mRecyclerView.setAdapter(mAdapter);


        return root;
    }


}


