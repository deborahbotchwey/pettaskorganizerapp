package com.rgajardo.pettaskorganizer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class AddTask extends AppCompatActivity {

    TextView time;
    SQLiteHelper db;
    Spinner spinnerNames;
    Button btnAddTask;
    RadioGroup btns;
    RadioButton radioButton;
    String taskType = "";
    String hours = "00";
    String minutes = "00";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);

        time = findViewById(R.id.simpleTimePicker);
        db = new SQLiteHelper(this);
        spinnerNames = findViewById(R.id.spinnerPetNames);
        btnAddTask = findViewById(R.id.btnAddTaskReady);
        btns = findViewById(R.id.radioGroupTasks);

        Cursor pets = db.getDataPets();
        ArrayList<String> data = new ArrayList<>();
        while(pets.moveToNext()){
            data.add(pets.getString(1));
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,data);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerNames.setAdapter(adapter);

        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);

        time.setText(hour + ":" + minute);

        time.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(AddTask.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        String ampm = "";
                        hours = getDD(selectedHour);
                        minutes = getDD(selectedMinute);
                        if (selectedHour > 12){
                            ampm = "PM";
                        }
                        else if (selectedHour < 12){
                            ampm = "AM";
                        }
                        time.setText(hours + ":" + minutes  +" " + ampm);
                    }
                }, 00 , 00, false);
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }});

        btns.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                radioButton = findViewById(checkedId);
                taskType = radioButton.getText().toString();

            }
        });

        }

    public void addTask(View view) {
        String name = spinnerNames.getSelectedItem().toString();
        String when = hours +":"+minutes;

        if (name == "" || taskType == "" || when.matches("00:00") ){
            Toast.makeText(this,"Complete all fields",Toast.LENGTH_LONG).show();
        }
        else{
            db.addDataTask(name,taskType,when);
            Toast.makeText(this,"new task added successfully",Toast.LENGTH_LONG).show();
            Intent intent = new Intent(AddTask.this, drawer.class);
            startActivity(intent);
        }
    }

    private String getDD(int num){
        return num > 9 ? "" + num : "0" + num;
    }
}


