package com.rgajardo.pettaskorganizer;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.LinkedList;

public class WordListAdapter extends
        RecyclerView.Adapter<WordListAdapter.MyViewHolder> {

    LinkedList<String> mData;
    FragmentManager mFm;
    SharedPreferences mPreferences;


    public WordListAdapter (LinkedList<String> wordList, FragmentManager fm) {
        mData = wordList;
        mFm = fm;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public final CheckBox cbox;

        final WordListAdapter mAdapter;
        public MyViewHolder (View itemView, WordListAdapter adapter){
            super(itemView);
            cbox = itemView.findViewById(R.id.chechBoxtask);
            this.mAdapter = adapter;
        }
    }

    @NonNull
    @Override
    public WordListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mItemView = (CheckBox) LayoutInflater.from(parent.getContext()).inflate(R.layout.taskitem,parent,false);
        MyViewHolder vh = new MyViewHolder(mItemView,this);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final String mCurrent = mData.get(position);
        holder.cbox.setText(mCurrent);
        holder.cbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean checked = ((CheckBox)v).isChecked();
                if (checked)
                openDialog(mCurrent);
            }
        });
    }
    public void openDialog(String data){
        DialogFragment dia = dialog.newInstance(data);
        dia.show(mFm, "done");
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


}

